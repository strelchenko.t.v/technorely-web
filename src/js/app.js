//= ../vendor/jquery/dist/jquery.min.js

//= ../../node_modules/bootstrap/js/dist/index.js
//= ../../node_modules/bootstrap/js/dist/util.js
//= ../../node_modules/bootstrap/js/dist/tab.js
//= ../../node_modules/bootstrap/js/dist/modal.js
//= ../../node_modules/bootstrap/js/dist/collapse.js
//= ../vendor/owl.carousel/dist/owl.carousel.min.js
//= ../vendor/jquery-validation/dist/jquery.validate.min.js
//= ../js/googlemap.js
//= ../js/scrolable.js


$(function(){

	var openMenuClass = 'open-menu';
	var headerPanel = $('.header');
	var mainContainer = $('body');
	var btn = headerPanel.find('.mobile-menu-btn');
	var win = $(window);
	var htmlBody = $('html, body');
	var section = $('.our-achievments-section');
	var animateClass = 'animate';
	var headerPanel = $('.header');
	var headerPanelHeight = headerPanel.outerHeight();
	var targetScrollTarget =  $('.our-achievments-section');
	var targetScrollPartners = $(".partners-section");
	var oldScrollTop = null;

	init();

	function init() {
		btnClickListener();
		initCarouselOwl();
		validateForm();
		fadeAnimation();
		changeBtnCollapse();
	}

	function initCarouselOwl() {
		$(".carousel-custom").each(function(index){
		const $carousel = $(this);
		const $dots = document.getElementsByClassName('customDots')[index];
		const $pages = document.getElementsByClassName('customNav')[index];
		const $current = document.getElementsByClassName('slide-current')[index];
		const $length = document.getElementsByClassName('slide-length')[index];

		$(this).owlCarousel({
			loop: true,
	        items: 1,
	        nav:true,
	        navContainer: $pages,
	        dotsContainer: $dots,
	        navText : ["",""],
	        onChange: function (elem) {
	            setTimeout(function(){
	                var count = elem.item.count;
	                var index = $carousel.closest('section').find('.owl-dots').find('.active').index() + 1;
	                if ($carousel.hasClass('init-carousel')) {
	                    if (index <= 9) index = '0' + index;
	                } else {
	                    $carousel.addClass('init-carousel');
	                    index = '01';
	                }
	                if (count <= 9) count = '0' + count;
	                $($length).html(count);
	                $($current).html(index);
	            });
		        }
		    });
		});

		$(".technologies-carousel").owlCarousel({
		    loop: true,
		    nav: true,
		    navContainer: '.technologies-nav',
		    dots: false,
		    items: 5,
		    margin: 30,

		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:3
		        },
		        1100:{
		            items:4
		        },
		        1400:{
		            items:5
		        }
		    }
		});

		$(".team-carousel").owlCarousel({
		    loop: true,
		    nav: true,
		    navContainer: '.team-slider-nav',
		    dots: false,
		    items: 3,
		    margin: 30,

		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		});
	}

    function validateForm() {
    	var validateOptions = {
	        errorClass: 'has-error',
	        validClass: 'has-success',

	        highlight: function(element, errorClass, validClass) {
	            $(element).parents(".form-group").addClass(errorClass).removeClass(validClass);
	        },

	        unhighlight: function(element, errorClass, validClass) {
	            $(element).parents(".form-group").removeClass(errorClass).addClass(validClass);
	        },

	        rules: {
	            name: {
	                required: true,
	                maxlength: 20
	            },

	            email: {
	                required: true,
	                email: true,
	                regex: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
	            },

	            phone: {
	                required: true,
	                number: true,
	                rangelength: [8, 15]
	            }
	        },

	         messages: {
	            name: "Name must be from 1 to 20 numbers",
	            email: "Please enter a valid email address",
	            phone: {
	                required: "Please enter a valid phone number",
	                number: "Please enter a valid phone number",
	                rangelength: "Phone must be from 8 to 15 numbers"
	            }
	        },

	        submitHandler: function(form) {
	            var $form = $(form);
	            var $inputs = $form.find('.form-control');
	            var serializedData = $form.serialize();

	            $('.modal-submit').modal('show').on('hidden.bs.modal', function() {
	                $form.validate().destroy();
	            });

	            $inputs.val('');

	            $(".form-group").removeClass('has-success');
	        }
	    }

	    $.validator.addMethod(
			"regex",
			function(value, element, regexp) {
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			},
			"Incorrect format; Please check your input."
		);


    	$('.form').validate(validateOptions);

    	$('.btn-validation').on('click', function(e) {

			if ($(e.target).closest('form').hasClass('advice-form')) {
				$('.advice-form').validate(validateOptions);
			} else {
				$('.contact-form').validate(validateOptions);
			}
			
		})
    }

	function btnClickListener() {
		if (btn.length) {
			btn.on('click', function () {
				mainContainer.toggleClass(openMenuClass);
			});
		}
	}

	function fadeAnimation() {
		win.on({
			'scroll load': function(e) {

				if (win.scrollTop() >= targetScrollTarget.offset().top - headerPanelHeight) {
					$('.numbers-list').addClass(animateClass);
				}

				if (win.scrollTop() >= targetScrollPartners.offset().top - headerPanelHeight) {
					$('.partners-list').addClass(animateClass);
				}

		// oldScrollTop = false;
			},
		// 	'mousewheel wheel DOMMouseScroll': function (e) {
		// 		htmlBody.stop();
		// 	}
		});
	}

	function changeBtnCollapse() {
		if ($('.collapsed-btn').hasClass('collapsed')) {
			$('.collapsed-btn').text('Close');
		}
	}

});
