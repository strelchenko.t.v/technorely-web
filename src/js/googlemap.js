const latLng = [
    {
        city: 'Canada',
        lat: 49.2636429,
        lng: -122.8853209,
        latCenter: 49.2636429,
        lngCenter:  -122.8853209,
        infowindow: '<div class="infoWindow">625 Como Lake Ave,<br/>Vancouver, British Columbia,<br/>Canada</div>',
    },
    {
        city: 'Kharkiv',
        lat: 49.994683,
        lng: 36.2361796,
        latCenter: 49.994683,
        lngCenter:  36.2361796,
        infowindow: '<div class="infoWindow">22 Pushkinska st.,<br/>office 8,<br/>Kharkiv, Ukraine</div>',
    },
    {
        city: 'Kyiv',
        lat: 50.4524635,
        lng: 30.4435972,
        latCenter: 50.4524635,
        lngCenter:  30.4435972,
        infowindow: '<div class="infoWindow">1B Vadym Hetman st.,<br/>Kyiv, Ukraine</div>',
    }
]

class googleMap {

    constructor () {
        this.panPath = [];   // An array of points the current panning action will use
        this.panQueue = [];  // An array of subsequent panTo actions to take
        this.STEPS = 55;
        this.sizeTimeOut = 260;
        this.sizeTimeIn = 160;
    }

    init() {
        this
            .initMap();
    }

    initMap(){
        const that = this;
        if ($('.map').length) {
            let features = [
                {
                    position: new google.maps.LatLng(latLng[0].lat, latLng[0].lng),
                },
                {
                    position: new google.maps.LatLng(latLng[1].lat, latLng[1].lng),
                },
                {
                    position: new google.maps.LatLng(latLng[2].lat, latLng[2].lng)
                }
            ];
            let centerMap = latLng[0].latCenter+', '+ latLng[0].lngCenter;
            const map = this.initializeMap(document.getElementById('map'), features, centerMap);
            $('.map-url').on('click', function(e){
                e.preventDefault();
                if(!$(this).hasClass('openMap')) {
                    const coordInd = $(this).attr('data-coordinate');
                    const lat = parseFloat(latLng[coordInd].lat);
                    const lng = parseFloat(latLng[coordInd].lng);
                    that.smoothZoomOut(map, 1, map.getZoom(), 0, map.getCenter());
                    const neighborsPrev = $('.openMap').attr('data-neighbors');
                    const neighborsNext = $(this).attr('data-neighbors');
                    google.maps.event.addListenerOnce(map, "idle", function() {
                        if (neighborsPrev === neighborsNext){
                            setTimeout(function(){
                                that.panTo(lat, lng, map, 10);
                            }, 2100);
                        } else {
                            setTimeout(function(){
                                that.panTo(lat, lng, map, that.STEPS);
                            }, 3100);
                        }
                    });
                    $('.openMap').removeClass('openMap');
                    $(this).addClass('openMap');
                }
            });
        }
        return this;
    }

    panTo(newLat, newLng, map, steps) {
        const that = this;
        console.log(789)
        if (this.panPath.length > 0) {
            // We are already panning...queue this up for next move
            this.panQueue.push([newLat, newLng]);
        } else {
            // Lets compute the points we'll use
            this.panPath.push("LAZY SYNCRONIZED LOCK");  // make length non-zero - 'release' this before calling setTimeout
            const curLat = map.getCenter().lat();
            const curLng = map.getCenter().lng();
            const dLat = (newLat - curLat)/steps;
            const dLng = (newLng - curLng)/steps;

            for (var i=0; i < steps; i++) {
                this.panPath.push([curLat + dLat * i, curLng + dLng * i]);
            }
            this.panPath.push([newLat, newLng]);
            this.panPath.shift();      // LAZY SYNCRONIZED LOCK
            setTimeout(function(){
                that.doPan(map, steps)
            },120);
        }
    }

    doPan(map, steps) {
        const that = this;
        var next = this.panPath.shift();
        if (next != null) {
            // Continue our current pan action
            map.panTo( new google.maps.LatLng(next[0], next[1]));
            setTimeout(function(){
                that.doPan(map, steps)
            },120);
        } else {
            // We are finished with this pan - check if there are any queue'd up locations to pan to
            var queued = this.panQueue.shift();
            if (queued != null) {
                that.panTo(queued[0], queued[1], steps);
            } else {
                that.smoothZoomIn (map, 15, map.getZoom())
            }
        }
    }

    // the smooth zoom function
    smoothZoomIn (map, max, cnt) {
        const that = this;
        if (cnt >= max) {
            return;
        }
        else {
            var z = google.maps.event.addListener(map, 'zoom_changed', function(event){
                google.maps.event.removeListener(z);
                that.smoothZoomIn(map, max, cnt + 1);
            });
            setTimeout(function(){map.setZoom(cnt)}, this.sizeTimeIn);
        }
    }

    smoothZoomOut (map, min, cnt, index, centerLatLng) {
        // const that = this;
        // if (cnt <= min) {
        //     if (cnt > 7) {
        //         that.smoothZoomOut(map, 6, map.getZoom(), index, centerLatLng);
        //     } else if (cnt > 4) {
        //         that.smoothZoomOut(map, 3, map.getZoom(), index, centerLatLng);
        //     } else if (cnt > 2) {
        //         that.smoothZoomOut(map, 1, map.getZoom(), index, centerLatLng);
        //     } else {
        //         return;
        //     }
        // }
        // else {
        //     const z = google.maps.event.addListener(map, 'zoom_changed', function(event){
        //         google.maps.event.removeListener(z);
        //         map.setCenter(centerLatLng);
        //         that.smoothZoomOut(map, min, cnt - 1, index, centerLatLng);
        //     });
        //
        //     if (cnt < 2) {
        //         setTimeout(function(){map.setZoom(cnt)});
        //     } else {
        //         setTimeout(function(){map.setZoom(cnt)}, this.sizeTimeOut);
        //     }
        // }

        const that = this;

        if (cnt <= min) {
            console.log(cnt, 'cnt')
            return;
        }
        else {
            index = index + 1;
            const z = google.maps.event.addListener(map, 'zoom_changed', function(event){
                google.maps.event.removeListener(z);
                console.log('center',centerLatLng)
                map.setCenter(centerLatLng);
                that.smoothZoomOut(map, min, cnt - 1, index, centerLatLng);
            });
            if(index === 1) {
                setTimeout(function(){map.setZoom(cnt)});
            } else {
                setTimeout(function(){map.setZoom(cnt)}, this.sizeTimeOut);
            }
        }
    }

    initializeMap(mapItem, coordinatesMap, centerMap) {
        if ($(mapItem).length) {
            const zoom = 15;
            const styledMapType = new google.maps.StyledMapType(
                [
                    {
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.province",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.locality",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "saturation": "-100"
                            },
                            {
                                "lightness": "30"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.neighborhood",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "gamma": "0.00"
                            },
                            {
                                "lightness": "74"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": "3"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    }
                ],
                {name: 'Styled Map'});
            const LatLng = centerMap.replace('(', '').replace(')', '').split(', '),
                Lat = parseFloat(LatLng[0]),
                Lng = parseFloat(LatLng[1]),
                mapOptions = {
                    zoom: zoom,
                    center: new google.maps.LatLng(Lat, Lng),
                    mapTypeControlOptions: {
                        mapTypeIds: []
                    }
                };
            const map = new google.maps.Map(mapItem, mapOptions);
            const pinImage = new google.maps.MarkerImage('images/pin.png');

            coordinatesMap.forEach(function (cootdinateMap, i) {
                const marker = new google.maps.Marker({
                    position: cootdinateMap.position,
                    icon: pinImage,
                    map: map,
                    draggable: false,
                });
                const contentString = latLng[i].infowindow;
                const infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    map: map,
                    position: new google.maps.LatLng(latLng[i].lat, latLng[i].lng),
                    pixelOffset: new google.maps.Size(165, 20),
                });

                google.maps.event.addListenerOnce(map, 'idle', function () {
                    $('.gm-style-iw').prev('div').remove();
                    setTimeout(function () {
                        $('.gm-style-iw').prev('div').remove();
                    });
                });

                const latMarker = cootdinateMap.position.lat();
                const lngMarker = cootdinateMap.position.lng();


            });
            //Associate the styled map with the MapTypeId and set it to display.
            map.mapTypes.set('styled_map', styledMapType);
            map.setMapTypeId('styled_map');
            return map;
        }
    }
}

$(function () {
    $(document).ready(function () {
        let GM = new googleMap();
        window.GM = GM;
        GM.init();
        window.$ = jQuery;
    });
});